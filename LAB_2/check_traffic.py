import csv

dns = open("data/dns.log")
hosts = open("data/hosts.txt")
meta_inf = csv.reader(dns, delimiter="\x09")
hosts_csv = csv.reader(hosts)

hosts_list = list()
for row in hosts_csv:
    try:
    	hosts_list.append(row[0])
    except Exception:
        pass

hosts_count = 0
for row in meta_inf:
    try:
        if row[9] in hosts_list:
            hosts_count += 1
            print("Host: " + row[9] + " is unwanted!")
    except IndexError:
        pass

print("Percentage of unwanted traffic is: " + str(round(hosts_count / (meta_inf.line_num - 8) * 100, 2)) + "%")

dns.close()
hosts.close()
